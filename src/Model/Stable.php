<?php

namespace App\Model;

use App\Model\Address;
use App\Model\Manager;


class Stable {

	private string $name;
	protected Address $address;
	protected Manager $manager;

	public function __construct(
		Address $address,
		string $stableName,
		Manager $manager)
	{
		
		$this->setName($stableName);
		$this->adress = $address;
		$this->manager = $manager;

	}

	/**
	 * Get the value of name
	 */ 
	public function getName()
	{
			return $this->name;
	}
	/**
	 * Set the value of name
	 *
	 * @return  self
	 */ 
	public function setName($name)
	{
			$this->name = $name;
			return $this;
	}

	
	
	/**
	 * Get the value of address
	 */ 
	public function getAddress()
	{
		return $this->address;
	}
	
	/**
	 * Set the value of address
	 *
	 * @return  self
	 */ 
	public function setAddress($address)
	{
		$this->address = $address;
		
		return $this;
	}
	
	/**
	 * Get the value of manager
	 */ 
	public function getManager()
	{
		return $this->manager;
	}
	
	/**
	 * Set the value of manager
	 *
	 * @return  self
	 */ 
	public function setManager($manager)
	{
		$this->manager = $manager;
		
		return $this;
	}
	public function __toString()
	{
			return $this->name." Adress : ".$this->adress->__toString()." Managed by : ".$this->manager->getName();
	}
}