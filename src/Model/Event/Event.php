<?php
namespace App\Model\Event;


use App\Model\Event\Team;
use App\Controller\EventController;

abstract class Event{

	private string $nameEvent;
	private int $maxCommitments;
	private int $maxWater;
	private string $category;
	private array $teams = [];

	public function __construct(string $nameEvent, int $maxCommitments, int $maxWater, string $category)
	{
		$this->setNameEvent($nameEvent)
			->setMaxCommitments($maxCommitments)
			->setMaxWater($maxWater)
			->setCategory($category);
			
	}
	
	/**
	 * Get the value of nameEvent
	 */ 
	public function getNameEvent()
	{
		return $this->nameEvent;
	}

	/**
	 * Set the value of nameEvent
	 *
	 * @return  self
	 */ 
	public function setNameEvent($nameEvent)
	{
		$this->nameEvent = $nameEvent;

		return $this;
	}

	/**
	 * Get the value of maxCommitments
	 */ 
	public function getMaxCommitments()
	{
		return $this->maxCommitments;
	}

	/**
	 * Set the value of maxCommitments
	 *
	 * @return  self
	 */ 
	public function setMaxCommitments($maxCommitments)
	{
		$this->maxCommitments = $maxCommitments;

		return $this;
	}

	/**
	 * Get the value of maxWater
	 */ 
	public function getMaxWater()
	{
		return $this->maxWater;
	}

	/**
	 * Set the value of maxWater
	 *
	 * @return  self
	 */ 
	public function setMaxWater($maxWater)
	{
		$this->maxWater = $maxWater;

		return $this;
	}

	/**
	 * Get the value of category
	 */ 
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * Set the value of category
	 *
	 * @return  self
	 */ 
	public function setCategory($category)
	{
		$this->category = $category;

		return $this;
	}

	

	/**
	 * Get the value of teams
	 */ 
	public function getTeams() :array
	{
		return $this->teams;
	}

	/**
	 * Set the value of teams
	 *
	 * @return  self
	 */ 
	public function setTeams($teams)
	{
		$this->teams = $teams;
		
		return $this;
	}
}