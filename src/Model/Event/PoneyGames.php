<?php

namespace App\Model\Event;

use Exception;
use App\Model\Event\Team;
use App\Model\Animal\Poney;
use App\Model\Animal\Sheitland;
use App\Controller\EventController;

class PoneyGames extends Event{

    public function __construct(string $nameEvent, int $maxCommitments, int $maxWater)
    {
	   parent::__construct($nameEvent, $maxCommitments, $maxWater, "PoneyGames");
    }
    

    /**
	* It adds a team to the competition if the equine is a poney or a sheitland
	* @param Team  $team - The team to add to the competition
	* 
	* Generated on 11/13/2022 Gwilymm
	*/
    public function addTeam(Team $team){

	if ($team->getEquine() instanceof Poney|| $team->getEquine() instanceof Sheitland) {

	    $teaminCompetitoion = $this->getTeams();
	    $waterUsed = $this->getMaxWater();
	    $maxTeamsReached = $this->getMaxCommitments();
	    $rider = EventController::checkRider($this, $team);
	    $maxTeamsReached -= EventController::maxTeamsReached($this, $team);
	    $teaminCompetitoion[] = $team;
	    $waterUsed -= EventController::checkWater($this, $team);
	    
	    $this->setTeams($teaminCompetitoion)->setMaxWater($waterUsed);
    }
    else{
	    throw new Exception("Can't add this equine he can't play Poneygames");
    }
    }
}

