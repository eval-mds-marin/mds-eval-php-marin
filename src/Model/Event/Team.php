<?php
namespace App\Model\Event;

use App\Model\Animal\Equine;
use App\Model\Rider;

class Team{


	private Equine $equine;
	private Rider $rider;
	private string $nameTeam;


	public function __construct(Equine $equine, Rider $rider)
	{
		$this->setEquine($equine)
			->setRider($rider);
	}
	
	/**
	 * Get the value of equine
	 */ 
	public function getEquine()
	{
		return $this->equine;
	}

	/**
	 * Set the value of equine
	 *
	 * @return  self
	 */ 
	public function setEquine($equine)
	{
		$this->equine = $equine;

		return $this;
	}

	/**
	 * Get the value of rider
	 */ 
	public function getRider()
	{
		return $this->rider;
	}

	/**
	 * Set the value of rider
	 *
	 * @return  self
	 */ 
	public function setRider($rider)
	{
		$this->rider = $rider;

		return $this;
	}

	/**
	 * Get the value of nameTeam
	 */ 
	public function getNameTeam()
	{
		return $this->nameTeam;
	}

	/**
	 * Set the value of nameTeam
	 *
	 * @return  self
	 */ 
	public function setNameTeam($nameTeam)
	{
		$this->nameTeam = $nameTeam;

		return $this;
	}

	public function __toString()
	{
		return "L'équipe est composée de ".$this->getRider()->getName()." et de ".$this->getEquine();
	}
}