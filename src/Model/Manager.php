<?php
namespace App\Model;

use App\Model\Human;

class Manager extends Human
{
    protected Address $address;
    private string $name;

    public function __construct(
        Address $address,
        string $name
    )
    {
        parent::__construct($address, $name);
    }

    
    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }
    public function __toString()
    {
        return $this->name." ".$this->address;
    }
}