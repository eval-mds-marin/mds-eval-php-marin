<?php
namespace App\Model\Animal;

use Exception;
use App\Model\Animal\Equine;
use App\Controller\EquineController;

class Horse extends Equine{
	public string $category;
	public array $cappabilities;


	public function __construct(string $name, string $color, int $water, array $cappabilities)
	{
		$this->setCategory()
			->setCappabilities($cappabilities);
		parent::__construct($name, $color, $water);
	}


	public function setCategory(): self
	{
		$this->category = "Horse";

		return $this;
	}

	public function getCategory(): string
	{
		return $this->category;
	}

	public function setCappabilities(array $cappabilities) :array 
	{
		$capa =  EquineController::checkCapabilities($cappabilities);

		if (in_array('PoneyGames', $capa)) {
			throw new Exception('Horse can\'t play PoneyGames');

			
		}
		return $this->capabilities = $capa;

	}
	
	/**
	 * Get the value of cappabilities
	 */ 
	public function getCappabilities() :array
	{
		return $this->capabilities;
	}


	public function __toString()
	{
		return "Horse : ".$this->getName()." ".$this->getColor()." ".$this->getWater()." ".$this->getId();
	}
}