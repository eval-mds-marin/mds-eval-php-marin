<?php
namespace App\Model\Animal;

use App\Model\Animal\Equine;
use App\Controller\EquineController;

class Poney extends Equine{

	public string $category;
	public array $cappabilities;


	public function __construct(string $name, string $color, int $water, array $cappabilities)
	{
		$this->setCategory()
			->setCappabilities($cappabilities);
		parent::__construct($name, $color, $water);
	}


	public function setCategory(): self
	{
		$this->category = "Poney";

		return $this;
	}
	/**
	 * Get the value of cappabilities
	 */ 
	public function getCappabilities() :array
	{
		return $this->cappabilities;
	}

	/**
	 * Set the value of cappabilities
	 *
	 * @return  self
	 */ 
	public function setCappabilities($cappabilities) : self
	{
		$this->cappabilities =  EquineController::checkCapabilities($cappabilities);

		return $this;
	}


	public function __toString()
	{
		return "Poney : ".$this->getName()." ".$this->getColor()." ".$this->getWater()." ".$this->getId();
	}
	

	
}