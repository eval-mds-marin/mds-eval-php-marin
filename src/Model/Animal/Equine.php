<?php
namespace App\Model\Animal;


use App\Controller\EquineController;

abstract class Equine Extends Animal{

	private static int $countEquine = 0;
	private string $color;
	private int $water;

	public function __construct(
		string $name, 
		string $color,
		int $water,
		
		)
	{
		parent::__construct($name);
		$this->setId($color, $name, self::$countEquine)
			->setWater($water)
			->setColor($color);
	}

	/**
	 * Get the value of water
	 */ 
	public function getWater()
	{
		return $this->water;
	}

	/**
	 * Set the value of water
	 *
	 * @return  self
	 */ 
	public function setWater($water)
	{
		$this->water = $water;

		return $this;
	}

	/**
	 * Get the value of color
	 */ 
	public function getColor()
	{
		return $this->color;
	}

	/**
	 * Set the value of color
	 *
	 * @return  self
	 */ 
	public function setColor(string $color) :self
	{
		$this->color = EquineController::checkColor($color);
		
		return $this;
	}

	/**
	 * Get the value of id
	 */ 
	public function getId()
	{

		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @return  self
	 */ 
	public function setId(string $color, string $name, int $countEquine) :self
	{
		$this->id = EquineController::caculateId($color, $name, $countEquine);
		self::$countEquine++;

		return $this;
	}
	
}