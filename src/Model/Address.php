<?php
namespace App\Model;

class Address{

	private string $address;
	private string $street;
	private string $postCode;
	private string $city;

	public function __construct(
		string $address,
		string $street,
		string $postcode,
		string $city)
	{
		$this->setAddress($address)
			->setStreet($street)
			->setPostCode($postcode)
			->setCity($city);
	}


	/**
	 * Get the value of address
	 */ 
	public function getAddress() :string 
	{
		return $this->address;
	}

	/**
	 * Set the value of address
	 *
	 * @return  self
	 */ 
	public function setAddress($address) : self
	{
		$this->address = $address;

		return $this;
	}

	/**
	 * Get the value of street
	 */ 
	public function getStreet()
	{
		return $this->street;
	}

	/**
	 * Set the value of street
	 *
	 * @return  self
	 */ 
	public function setStreet($street)
	{
		$this->street = $street;

		return $this;
	}

	/**
	 * Get the value of postCode
	 */ 
	public function getPostCode()
	{
		return $this->postCode;
	}

	/**
	 * Set the value of postCode
	 *
	 * @return  self
	 */ 
	public function setPostCode($postCode)
	{
		$this->postCode = $postCode;

		return $this;
	}

	/**
	 * Get the value of city
	 */ 
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * Set the value of city
	 *
	 * @return  self
	 */ 
	public function setCity($city)
	{
		$this->city = $city;

		return $this;
	}
	public function __toString()
	{
		return $this->getAddress() . " " . $this->getStreet() . " " . $this->getPostCode() . " " . $this->getCity();
	}
}