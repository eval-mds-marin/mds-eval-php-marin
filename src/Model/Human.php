<?php
namespace App\Model;

use App\Model\Address;

abstract class Human {

	private string $name;
	protected Address $address;

	public function __construct(
		Address $address,
		string $name)
	{
		
		$this->setName($name);
		$this->address = $address;

	}


	/**
	 * Get the value of name
	 */ 
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set the value of name
	 *
	 * @return  self
	 */ 
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	public function __toString()
	{
		return $this->name." ".$this->address;
	}
}