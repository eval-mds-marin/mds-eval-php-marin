<?php
namespace App\Controller;

use Exception;
use App\Model\Rider;
use App\Model\Event\Team;
use App\Model\Event\Event;
use App\Model\Animal\Equine;


class EventController{

	/**
	 * It checks if the number of teams is less than the maximum number of teams allowed for the event,
	 * and if it is, it increments the number of teams by one
	 * @param Event  $event - The event object
	 * @param Team  $team - The team that is being added to the event
	 * @returns The number of teams
	 * 
	 * Generated on 11/13/2022 Gwilymm
	 */
	public static function maxTeamsReached(Event $event , Team $team){

		$maxTeams = $event->getMaxCommitments();
		$teams = count($event->getTeams());
		
		if ($teams < $maxTeams) {
		    $teams++;
		}else{
			throw new Exception("Le nombre maximum de participants est atteint");
		}
		return $teams;

	}

	/**
	 * It checks to see if the amount of water used by the equines in the team is less than the maximum
	 * amount of water allowed for the event
	 * @param Event  $event - The event object
	 * @param Team  $team - The team object
	 * @returns The water used by the equine.
	 * 
	 * Generated on 11/13/2022 Gwilymm
	 */
	public static function checkWater(Event $event , Team $team){
		$maxWater = $event->getMaxWater();
		$waterUsed = 0;
		$waterUsed += $team->getEquine()->getWater();

		if ($waterUsed >= $maxWater) {

			throw new Exception("Can't add more equines not enough water");

	    } else {

	        return $waterUsed;

	    }
	}

	/***
	 * Check if the rider ride less than 5 horses in the event
	 * @param Event  $event - The event object
	 * @param Team  $team - The team object
	 * @returns The number of horses ridden by the rider
	 */
	public static function checkRider(Event $event , Team $team){
		$nbHorses = 0;
		$teams = $event->getTeams();
		$rider = $team->getRider();
		foreach ($teams as $team) {
			if ($team->getRider() == $rider) {
				$nbHorses++;
			}
		}
		if ($nbHorses >= 5) {
			throw new Exception("Can't accepte this teams the rider ride too much horses");
		}else{
			return $nbHorses;
		}
	}
}
