<?php
namespace App\Controller;

use Exception;

class EquineController {
   
    /**
     * It takes in a color, name, and countequine and returns a string.
     * @param string  $color - The color of the horse.
     * @param string  $name - The name of the horse
     * @param int  $countequine - This is the number of equines in the database.
     * @returns the id of the horse.
     * 
     * Generated on 11/13/2022 Gwilymm
     */
     public static function caculateId(string $color, string $name, int $countequine): string
     {
         $id ="000-".strtolower($name[0])."-".$color[0]."-".$countequine;

         return $id;
     }

    /**
     * It checks if the color is valid, and if it is, it returns it
     * @param string  $color - The color of the cat.
     * @returns the color if it is valid.
     * 
     * Generated on 11/13/2022 Gwilymm
     */
    public static function checkColor(string $color)
    {
        $possibleColor = array('Alzan', 'Bai', 'Pie', 'Grey', 'White');
        if (in_array($color, $possibleColor)) {
            $checkedColor = $color;
        } else {
            throw new Exception("La couleur n'est pas valide");
        }
        return $checkedColor;
    }

    /**
     * It checks if the values in the array  are in the array , and if
     * they are, it adds them to the array 
     * @param array  $cappabilities - an array of strings, each string being a cappability
     * @returns an array of checked cappabilities.
     * 
     * Generated on 11/13/2022 Gwilymm
     */
    public static function checkCapabilities(array $cappabilities)
    {
        $possibleCappabilities = array('Saut', 'Dressage', 'Cross', 'PoneyGames');
        foreach ($cappabilities as $cappability) {
            if (in_array($cappability, $possibleCappabilities)) {
                $checkedCappabilities[] = $cappability;
            } else {
                throw new Exception("La capacité n'est pas valide");
            }
        }
        return $checkedCappabilities;
    }


}