<?php


use App\Model\Rider;
use App\Model\Stable;
use App\Model\Address;
use App\Model\Manager;
use App\Model\Event\Team;
use App\Model\Animal\Horse;
use App\Model\Animal\Poney;
use App\Model\Animal\Sheitland;
use App\Model\Event\PoneyGames;


// Autoloading, pas besoin de require partout pour charger nos classes et chargement
// automatique de nos librairies
require_once __DIR__.'/../vendor/autoload.php';

// Définitions de chemins utiles dans l'application
define('TEMPLATES_DIR', __DIR__ . '/../templates/');
define('SRC_DIR', __DIR__ . '/');
define('PUBLIC_DIR', __DIR__ . '/../public/');

echo "<h2> My Little Poney App </h2>";

$managerAdress = new Address('France', '75000', 'Paris', 'Rue de la Paix', '1');

$manager = new Manager($managerAdress, "Malik");

echo $manager."<br>";

$address = new Address("12 rue","de geole","14000","Caen");

$stable = new Stable($address,"écurie du soleil",$manager);

echo $stable."<br>";

$addressRider = new Address("12 rue","basse","14000","Caen");

$rider = new Rider($addressRider, "Maxime");

echo $rider."<br>";

$poney1 = new Poney("Fleur de biscotte",'Alzan',75,['PoneyGames']);


try {
    echo "<h3>When horse try to play PoneyGames !</h3>";
    $horse1 = new Horse("Super cheval", 'Bai', 150, ['Saut', 'Dressage', 'Cross', 'PoneyGames']);
}catch(Exception $e){
    echo $e->getMessage()."<br>";
}

try {
    $horse2 = new Horse("Super cheval", 'Bai', 150, ['Saut', 'Dressage', 'Cross']);
}catch(Exception $e){
    echo $e->getMessage()."<br>";
}
echo $horse2."<br>";

$sheitland1 = new Sheitland("Indiscutable Poney",'Pie',50, ['Saut', 'Dressage', 'Cross']);

$teamBlue = new Team($poney1, $rider,"Blue Team");

$teamRed = new Team($sheitland1, $rider,"Red Team");

$greatEvent = new PoneyGames("Great Event", 10, 100);

$greatEvent->addTeam($teamBlue);


try {
    echo "<h3>Add too much horse to event no water left !</h3>";
    echo $teamRed."<br>";
    $greatEvent->addTeam($teamRed);
}catch(Exception $e){    
    echo $e->getMessage() . "<br>";
    echo "Water left : ".$greatEvent->getMaxWater() . "<br>";
}


try{
    echo "<h3>Add Hosre to PoneyGames</h3>";
    $teamHorse = new Team($horse2, $rider, "Horse Team");
    echo $teamHorse."<br>";
    $greatEvent->addTeam($teamHorse);
}
catch(Exception $e){
    echo $e->getMessage();
}

try{
    echo "<h3>Add too much team to event</h3>";
    $poney3 = new Poney("Fleur de biscotte",'Alzan',20,['PoneyGames']);
    $teamPoneyRed = new Team($poney3, $rider, "Poney Team");
    $eventSmall= new PoneyGames("Small Event", 1, 100);
    $eventSmall->addTeam($teamPoneyRed);
    $eventSmall->addTeam($teamPoneyRed);
    
}
catch(Exception $e){
    echo $e->getMessage();
}

try {
    echo "<h3>Check rider ride less than 5 horses</h3>";
    $poney4 = new Poney("Fleur de biscotte",'Alzan',20,['PoneyGames']);
    $teamPoneyRed = new Team($poney4, $rider, "Poney Team");
    $eventBig= new PoneyGames("Big Event", 10, 1000);
    $eventBig->addTeam($teamPoneyRed);
    $eventBig->addTeam($teamPoneyRed);
    $eventBig->addTeam($teamPoneyRed);
    $eventBig->addTeam($teamPoneyRed);
    $eventBig->addTeam($teamPoneyRed);
    $eventBig->addTeam($teamPoneyRed);
    
    
}
catch(Exception $e){
    echo $e->getMessage();
}










